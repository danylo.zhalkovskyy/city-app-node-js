import test from 'ava';

const axios = require('axios');

// Exemple :
test('foo', t => {
	t.pass();
});

test('bar', async t => {
	const bar = Promise.resolve('bar');
	t.is(await bar, 'bar');
});

// Test de la ville chercher
test('verifier ville', v => {
      v.pass()
});

test('verif ville',  async v => {
	const ville = Promise.resolve('ville');
      v.is(await ville, 'ville');
});

test('index page contains a form', async t => {
  const response = await axios.get(`${urlPrefix}/`);
  const htmlPage = response.data;
  t.regex(htmlPage, /<form/);
});
